//
//  AgendaAppDelegate.h
//  Agenda
//
//  Created by TCS - Aviva on 29/04/14 (Saurav Sharma).
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
