//
//  AgendaContactsDetailsViewController.h
//  Agenda
//
//  Created by TCS - Aviva on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaContactsDetailsViewController : UIViewController<UISplitViewControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (retain, nonatomic) id contactDetailItem;
@end
