//
//  AgendaContactsDetailsViewController.m
//  Agenda
//
//  Created by TCS - Aviva on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaContactsDetailsViewController.h"

@interface AgendaContactsDetailsViewController ()
{
    IBOutlet UITableView *contactDetailTable;
}
@end

@implementation AgendaContactsDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setContactDetailItem:(id)newContactDetailItem
{
    if (_contactDetailItem != newContactDetailItem) {
        _contactDetailItem = newContactDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

-(void)configureView
{
    if (self.contactDetailItem) {
        [contactDetailTable reloadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((NSArray *)(self.contactDetailItem)).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactDetail_Cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *contactDict = [((NSArray *)(self.contactDetailItem)) objectAtIndex:indexPath.row];
    ((UILabel *)[cell viewWithTag:101]).text = [contactDict objectForKey:@"Name"];
    ((UILabel *)[cell viewWithTag:102]).text = [contactDict objectForKey:@"Role"];
    ((UILabel *)[cell viewWithTag:103]).text = [contactDict objectForKey:@"Number"];
    ((UILabel *)[cell viewWithTag:104]).text = [contactDict objectForKey:@"Mail"];
    return cell;
}

@end
