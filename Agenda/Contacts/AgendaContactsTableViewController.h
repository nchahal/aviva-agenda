//
//  AgendaContactsTableViewController.h
//  Agenda
//
//  Created by TCS - Aviva on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgendaContactsDetailsViewController.h"

@interface AgendaContactsTableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) AgendaContactsDetailsViewController *contactDetailViewController;

@end
