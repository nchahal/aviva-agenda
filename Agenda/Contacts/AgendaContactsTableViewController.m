//
//  AgendaContactsTableViewController.m
//  Agenda
//
//  Created by TCS - Aviva on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaContactsTableViewController.h"
#import "AgendaContactsDetailsViewController.h"

@interface AgendaContactsTableViewController ()
{
    NSArray *contactsMainArray;
}
@end

@implementation AgendaContactsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.view.backgroundColor =
    [UIColor colorWithPatternImage:[UIImage imageNamed:@"masterBackground.png"]];
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"ContactsList" ofType:@"plist"];
    contactsMainArray = [[NSArray alloc] initWithContentsOfFile:resourcePath];
    
    self.contactDetailViewController = (AgendaContactsDetailsViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.contactDetailViewController.contactDetailItem = [contactsMainArray objectAtIndex:0];
    
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return contactsMainArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contact_TableCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.textLabel.text = [[[contactsMainArray objectAtIndex:indexPath.row] objectAtIndex:0] valueForKeyPath:@"Place"];
    
    
    return cell;
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:2.0/255.0 green:90.0/255.0 blue:180.0/255.0 alpha:1.0];
    [tableView cellForRowAtIndexPath:indexPath].textLabel.textColor = [UIColor whiteColor];
    self.contactDetailViewController.contactDetailItem = [contactsMainArray objectAtIndex:indexPath.row];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
