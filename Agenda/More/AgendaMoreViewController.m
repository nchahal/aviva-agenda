//
//  AgendaMoreViewController.m
//  Agenda
//
//  Created by Saurav Sharma on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaMoreViewController.h"
#import "AgendaPDFViewController.h"

@interface AgendaMoreViewController ()
{
    NSArray *optionsArray;
    NSArray *pdfName;
}
@end

@implementation AgendaMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    optionsArray= [[NSArray alloc] initWithObjects:@"TATA",@"TCS",@"TCS Insurance", nil];
    pdfName= [[NSArray alloc] initWithObjects:@"A Dozen Ways to leverage value from BigData",
              @"The Emerging Big Returns on Big Data",
              @"Five Mobile Fails",
              @"Digital Enterprise- Framework for Transformation",
              @"The 3rd Platform- Enabling Digital Transformation",
              @"Building the Digital Insurance Enterprise for the Era of Engagement and Collaboration" ,
              @"The Digital Enterprise and the Changing Role of IT",
              @"Mastering Digital Feedback",
              nil];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return optionsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"options_Cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
//    if (indexPath.row == optionsArray.count-1) {
//        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LOCATION-ICON.png"]];
//    }
//    else {
//        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"05PDF_ICON.png"]];
//    }
    ((UILabel *)[cell viewWithTag:100]).text = [@"About " stringByAppendingString:[optionsArray objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Load_PDF" sender:[optionsArray objectAtIndex:indexPath.row]];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Load_PDF"]) {
        AgendaPDFViewController *pdfViewer = (AgendaPDFViewController *)[segue destinationViewController];
        pdfViewer.pdfName = sender;
    }
}

-(IBAction)viewJournal:(id)sender
{
    UIButton * senderButton = (UIButton *)sender;
    
    [self performSegueWithIdentifier:@"Load_PDF" sender:[pdfName objectAtIndex:senderButton.tag-1]];
}

@end
