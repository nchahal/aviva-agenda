//
//  AgendaPDFViewController.h
//  Agenda
//
//  Created by Saurav Sharma on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaPDFViewController : UIViewController

@property (nonatomic,retain) NSString *pdfName;
@end
