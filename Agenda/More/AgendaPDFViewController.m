//
//  AgendaPDFViewController.m
//  Agenda
//
//  Created by Saurav Sharma on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaPDFViewController.h"

@interface AgendaPDFViewController ()
{
    IBOutlet UIWebView* webView;
}
@end

@implementation AgendaPDFViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.pdfName;
    NSLog(@"%@",self.pdfName);
	// Do any additional setup after loading the view.
    NSString *path = [[NSBundle mainBundle] pathForResource:self.pdfName ofType:@"pdf"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    [webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
