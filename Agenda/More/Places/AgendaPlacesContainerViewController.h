//
//  AgendaPlacesContainerViewController.h
//  Agenda
//
//  Created by Saurav Sharma on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaPlacesContainerViewController : UIViewController<UIScrollViewDelegate>{
    BOOL pageControlUsed;
}
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) NSMutableArray *viewControllers;

- (IBAction)changePage:(id)sender;

@end
