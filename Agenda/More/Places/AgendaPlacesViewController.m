//
//  AgendaPlacesViewController.m
//  Agenda
//
//  Created by Saurav Sharma on 30/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaPlacesViewController.h"

static NSArray *__pageControlImageList = nil;
static NSArray *__pageControlPlaceList = nil;
static NSArray *__pageControlPlaceDetailsList = nil;
static NSArray *__pageControlPlaceVisitingScheduleList = nil;
static NSArray *__pageControlLocationList = nil;

@interface AgendaPlacesViewController ()
@property(nonatomic,strong) IBOutlet UIImageView *placeImage;
@property(nonatomic,strong) IBOutlet UILabel *place;
@property(nonatomic,strong) IBOutlet UILabel *location;
@property(nonatomic,strong) IBOutlet UITextView *placeDetails;
@end

@implementation AgendaPlacesViewController

+ (UIImage *)pageControlImageWithIndex:(NSUInteger)index {
    if (__pageControlImageList == nil) {
        __pageControlImageList = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"bangalorePalace.png"], [UIImage imageNamed:@"mysore.png"],[UIImage imageNamed:@"iskon.png"],[UIImage imageNamed:@"tippu_palace.png"],[UIImage imageNamed:@"nandi.png"], [UIImage imageNamed:@"shivanasamudra.png"], [UIImage imageNamed:@"wild_bennarghatta.png"],[UIImage imageNamed:@"wild_ranganathittu.png"],nil];
    }
    return [__pageControlImageList objectAtIndex:index % [__pageControlImageList count]];
}
+ (NSString *)pageControlPlaceWithIndex:(NSUInteger)index {
    if (__pageControlPlaceList == nil) {
        __pageControlPlaceList = [[NSArray alloc] initWithObjects:@"Bangalore Palace", @"Mysore Palace", @"Iskcon Temple", @"Tipu Sultan Palace", @"Nandi Hills", @"Shivanasamudra Falls", @"Bennarghatta National Park",@"Ranganathittu Bird Sanctury",nil];
    }
    return [__pageControlPlaceList objectAtIndex:index % [__pageControlPlaceList count]];
}

+ (NSString *)pageControlLocationWithIndex:(NSUInteger)index {
    if (__pageControlLocationList == nil) {
        __pageControlLocationList = [[NSArray alloc] initWithObjects:@"Bangalore", @"Mysore", @"Bangalore", @"Bangalore", @"Bangalore", @"Bangalore", @"Bangalore",@"Bangalore", nil];
    }
    return [__pageControlLocationList objectAtIndex:index % [__pageControlLocationList count]];
}

+ (NSString *)pageControlPlaceDetailsWithIndex:(NSUInteger)index {
    if (__pageControlPlaceDetailsList == nil) {
        __pageControlPlaceDetailsList = [[NSArray alloc] initWithObjects:
                                         @"Modelled on the lines of the Windsor Castle in England, the Bangalore Palace flaunts turreted parapets, battlements, fortified towers, and arches.",
                                         @"The Palace of Mysore (also known as the Amba Vilas Palace) is a palace situated in the city of Mysore in southern India. It is the official residence of the Wodeyars - the erstwhile royal family of Mysore, and also houses two durbar halls (ceremonial meeting hall of the royal court).\n\n Mysore is commonly described as the City of Palaces, however, the term \"Mysore Palace\" specifically refers to one within the old fort. The Wodeyar kings first built a palace in Mysore in the 14th century, it was demolished and constructed multiple times. The current palace construction was commissioned in 1897, and it was completed in 1912 and expanded later around 1940. \n\n Mysore palace is now one of the most famous tourist attractions in India after Taj Mahal with more than 2.7 million visitors",
                                         @"Built in an ornate architectural style, the Krishna Temple is a blend of modern technology and spiritual harmony.",
                                         @"A visit to Tipu's Fort is an enriching experience. Built in 1791, this summer retreat of Tipu Sultan in Bangalore is a two-storied ornate wooden structure with fluted pillars, cusped arches and balconies. It now houses a museum, which contains artefacts relating to the Hyder-Tipu regime.",
                                         @"This popular weekend getaway is just 60km from Bangalore. The bracing air and serene environs of Nandi Hills, perched at a height of 1455m above sea level, provided Tipu Sultan and the British with an idyllic summer retreat.",
                                         @"Discover nature's handiwork in the form of this tiny island-town, 65km east of Mysore. Forested hills and lush green valleys cradle a small hamlet and two fine temples. Together, they provide a startlingly calm setting for the Cauvery River as it plummets from a height of 75m into a deep, rocky gorge with a deafening roar to form two picturesque falls, Barachukki and Gaganachukki. When the Cauvery is in spate, watching the river crash into a cloud of foaming spray is an exhilarating experience. During the monsoon (June-September), the falls are at their impressive best. Downstream from the falls is Asia's first hydroelectric project, established in 1902.",
                                         @"For a walk on the wild side, look no further than the southern outskirts of Bangalore city, where you can find everything from avifauna to panthers in the Bannerghatta National Park. The 25,000 acre park is home to panthers, lions, tigers, and a large variety of birds. Indulge your sense of adventure with a lion and tiger safari for a tete-a-tete with the big cats. You could also wander through the Zoological Garden, with its canopy of shady and sturdy trees, find a quiet resting spot beside a pond and watch waterfowl frolic. The zoo boasts of an amazing reptile collection; a snake park that lets you get upclose and personal with scaly, slithery creatures. A children's corner provides an added attraction. Trekking enthusiasts will enjoy Uddigebande (3.5km) a natural rock formation called Hajjamana Kallu (3km) and Mirza Hill (1.5km)",
                                         @"Just outside Srirangapatna, the Cauvery River meanders around a string of tiny islets, which together form a splendid nesting site for birds. Experience a boat ride that takes you within touching distance of the birds, as marsh crocodiles bask in the sun. Delight in watching the winged visitors making happy forays into the water. You can also test your observation skills by trying to spot flying foxes hanging from the trees at dusk.",nil];
    }
    return [__pageControlPlaceDetailsList objectAtIndex:index % [__pageControlPlaceDetailsList count]];
}

+ (NSString *)pageControlPlaceVisitingScheduleWithIndex:(NSUInteger)index {
    if (__pageControlPlaceVisitingScheduleList == nil) {
        __pageControlPlaceVisitingScheduleList = [[NSArray alloc] initWithObjects:@"-", @"-", @"20th July,2012 (Friday)  to  26th July,2012 (Thursday)",@"22nd July, 2012 (Sunday)                                                       Time: 10 am to 4 pm",@"21st July, 2012 (Saturday)                                                       Time: 2 pm to 4 pm",@"23rd July,2012 (Monday)  to  26th July,2012 (Thursday)",@"27th July,2012 (Friday)  &  31st July,2012 (Tuesday)", nil];
    }
    return [__pageControlPlaceVisitingScheduleList objectAtIndex:index % [__pageControlPlaceVisitingScheduleList count]];
}


// Load the view nib and initialize the pageNumber ivar.
- (id)initWithPageNumber:(int)page {
    if (self = [super initWithNibName:@"AgendaPlacesViewController" bundle:nil]) {
        pageNumber = page;
    }
    return self;
}
// Set the label and background color when the view has finished loading.
- (void)viewDidLoad {
    
    // pageNumberLabel.text = [NSString stringWithFormat:@"Page %d", pageNumber + 1];
    self.placeImage.image = [AgendaPlacesViewController pageControlImageWithIndex:pageNumber];
    //self.placeImage.layer.cornerRadius = 8;
    self.placeImage.layer.masksToBounds = YES;
    [self.placeImage.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.placeImage.layer setBorderWidth: 1.0];
    
    self.place.text = [AgendaPlacesViewController pageControlPlaceWithIndex:pageNumber];
    self.location.text = [AgendaPlacesViewController pageControlLocationWithIndex:pageNumber];
    self.placeDetails.text = [AgendaPlacesViewController pageControlPlaceDetailsWithIndex:pageNumber];
    //schedule.text = [PlacesPageControlViewController pageControlPlaceVisitingScheduleWithIndex:pageNumber];
}

@end
