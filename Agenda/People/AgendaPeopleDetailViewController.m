//
//  AgendaContactDetailViewController.m
//  Agenda
//
//  Created by TCS - Aviva on 29/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaPeopleDetailViewController.h"

@interface AgendaPeopleDetailViewController ()
{
    IBOutlet UILabel *lblName;
    IBOutlet UIImageView *imageView;
    IBOutlet UILabel *lblDesignation;
    IBOutlet UILabel *lblOrganisation;
    IBOutlet UITextView *txtDescription;
}
- (IBAction)done:(id) sender;
- (IBAction)mail:(id)sender;
@end

@implementation AgendaPeopleDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[toolbar setBackgroundImage:[UIImage imageNamed:@"Tool-BAR.png"] forToolbarPosition:UIBarPositionTop barMetrics:UIBarMetricsDefault];
    //[toolbar setBarTintColor:[UIColor redColor]];
    // Do any additional setup after loading the view.
    lblName.text = [self.peopleDict objectForKey:@"Name"];
    lblDesignation.text = [self.peopleDict objectForKey:@"Designation"];
    lblOrganisation.text = [self.peopleDict objectForKey:@"Organization"];
    txtDescription.text = [self.peopleDict objectForKey:@"Description"];
    imageView.image = [UIImage imageNamed:[self.peopleDict objectForKey:@"Image"]];
    
    if (imageView.image == nil) {
        imageView.image = [UIImage imageNamed:@"PROFILE_IMG.png"];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)done:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)mail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.navigationBar.tintColor = [UIColor colorWithRed:229.0/255.0 green:184.0/255.0 blue:39.0/255.0 alpha:1.0];
        mailViewController.mailComposeDelegate = self;
       [mailViewController setToRecipients:[NSArray arrayWithObjects:[self.peopleDict valueForKey:@"Email"], nil]];
//        [mailViewController setSubject:@"<Name of patient> - Need Help with"];
        // SHOWING MAIL VIEW
        mailViewController.view.userInteractionEnabled = YES;
        [self presentViewController:mailViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *emailAlert = [[UIAlertView alloc] initWithTitle:@"Email Alert!" message:@"The application cannot compose Email with the current setting!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [emailAlert show];
    }
}

#pragma mark - MFMailComposer Delegates
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)erro
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
