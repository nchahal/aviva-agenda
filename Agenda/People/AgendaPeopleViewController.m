//
//  AgendaContactsViewController.m
//  Agenda
//
//  Created by TCS - Aviva on 29/04/14 (Saurav Sharma).
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaPeopleViewController.h"
#import "AgendaPeopleDetailViewController.h"

#define kNameLabelTag 100
#define kDesignationLabelTag 101


@interface AgendaPeopleViewController ()
{
    NSArray *peopleArray;
}
@end

@implementation AgendaPeopleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"PeopleList" ofType:@"plist"];
    peopleArray = [[NSArray alloc] initWithContentsOfFile:resourcePath];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Segue 
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Modal_AgendaContactDetails"]) {
        AgendaPeopleDetailViewController *contactDetails = (AgendaPeopleDetailViewController *)segue.destinationViewController;
        contactDetails.peopleDict = (NSDictionary *)sender;
    }
}

#pragma mark - UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return [peopleArray count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"contactCell" forIndexPath:indexPath];
    NSDictionary *peopleDict = [peopleArray objectAtIndex:indexPath.item];
    ((UILabel *)[cell viewWithTag:kNameLabelTag]).text = [peopleDict objectForKey:@"Nickname"];
    ((UILabel *)[cell viewWithTag:kDesignationLabelTag]).text = [peopleDict objectForKey:@"Designation"];
    ((UIImageView *)[cell viewWithTag:102]).image = [UIImage imageNamed:[peopleDict objectForKey:@"Image"]];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *headerView;
    return headerView;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Modal_AgendaContactDetails" sender:[peopleArray objectAtIndex:indexPath.item]];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if(self.sharing)
//    {
//        NSString *searchTerm = self.searches[indexPath.section];
//        FlickrPhoto *photo = self.searchResults[searchTerm][indexPath.row];
//        [self.selectedPhotos removeObject:photo];
//    }
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *searchTerm = self.searches[indexPath.section];
//    FlickrPhoto *photo = self.searchResults[searchTerm][indexPath.row];
//    CGSize retval = photo.thumbnail.size.width > 0 ? photo.thumbnail.size : CGSizeMake(100, 100);
//    retval.height += 35;
//    retval.width += 35;
//    return retval;
//}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(50, 20, 50, 20);
//}

@end
