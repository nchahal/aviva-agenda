//
//  AgendaScheduleViewController.h
//  Agenda
//
//  Created by TCS - Aviva on 29/04/14 (Saurav Sharma).
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaScheduleViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>{
    IBOutlet UITableView *scheduleTableView;
    IBOutlet UITextView *meetingDescription;
    IBOutlet UILabel *eventName;
    IBOutlet UILabel *facilitator;
    IBOutlet UILabel *place;
    IBOutlet UILabel *meetingTime;
    IBOutlet UILabel *noImagesLabel;
    NSMutableArray *peopleAtMeetingArray;
    IBOutlet UIScrollView *peopleImagesScrollView;
}
-(void)addImagesToScrollView:(NSArray *)imageArray withNames:(NSArray *)namesArray;
//-(void)profileSelectionAction:(id)sender;
@end
