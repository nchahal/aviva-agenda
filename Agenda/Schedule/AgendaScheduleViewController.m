//
//  AgendaScheduleViewController.m
//  Agenda
//
//  Created by TCS - Aviva on 29/04/14 (Saurav Sharma).
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaScheduleViewController.h"
#import "ScheduleTableViewCustomCell.h"

@interface AgendaScheduleViewController (){
    NSIndexPath *selectedIndexPath;
}

@property(nonatomic,retain) NSArray *scheduleDetailsArray;

@property(nonatomic,retain) IBOutlet UIView *presentationView;
@property(nonatomic,retain) IBOutlet UIView *agendaView;
@property(nonatomic,retain) IBOutlet UIWebView *webView;
@property(nonatomic,retain) IBOutlet UIView *animationContainer;

-(IBAction)presentationButton:(id)sender;
-(IBAction)cancelButton:(id)sender;
@end

@implementation AgendaScheduleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"ScheduleDetails" ofType:@"plist"];
    NSArray *scheduledetailsArray = [[NSArray alloc] initWithContentsOfFile:resourcePath];
    self.scheduleDetailsArray = scheduledetailsArray;
        
    [scheduleTableView  selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
    [self tableView:scheduleTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    // Do any additional setup after loading the view from its nib.
    //__________________________________
    NSString *path = [[NSBundle mainBundle] pathForResource:@"TATA" ofType:@"pdf"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    [self.webView loadRequest:request];
    //__________________________________
    [self.agendaView setHidden:FALSE];
    [self.presentationView setHidden:TRUE];
}

- (void)viewDidAppear:(BOOL)animated {
    //NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    //[scheduleTableView selectRowAtIndexPath:indexPath animated:YES  scrollPosition:UITableViewScrollPositionBottom];
}

-(void) viewWillAppear:(BOOL)animated {
    //[self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    //[self.navigationController setNavigationBarHidden:NO];
   // [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma TableView Delegate Methods- (NSInteger)numberOfSections;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.scheduleDetailsArray count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionHeaderName = [[[self.scheduleDetailsArray objectAtIndex:section] objectAtIndex:0] objectForKey:@"Date"];
    return sectionHeaderName;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.scheduleDetailsArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //static NSString *MyIdentifier = @"ScheduleTableViewCellIdentifier";
    ScheduleTableViewCustomCell *cell = (ScheduleTableViewCustomCell *)[tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ScheduleTableViewCustomCell" owner:nil options:nil] objectAtIndex:0];
    }
    
    NSDictionary *scheduleDict = [[self.scheduleDetailsArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    cell.meeting.text = [scheduleDict objectForKey:@"Topic"];
    cell.place.text = [scheduleDict objectForKey:@"Location"];
    cell.time.text = [scheduleDict objectForKey:@"Time"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.layer.masksToBounds = YES;
    cell.backgroundColor = [UIColor clearColor];
    
//    if (cell.selected) {
//        cell.contentView.backgroundColor = [UIColor colorWithRed:252.0/255.0 green:224.0/255.0 blue:35.0/255.0 alpha:1.0];
//    }
//    else {
//        cell.contentView.backgroundColor = [UIColor whiteColor];
//    }
    return cell;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    view1.backgroundColor =[UIColor blackColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13, 7, tableView.frame.size.width, 26)];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor =[UIColor clearColor];
    view1.layer.masksToBounds = YES;
    [view1.layer setBorderColor:[[UIColor clearColor] CGColor]];
    [view1.layer setBorderWidth: 1.0];
    
    [view1 addSubview:label];
    return  view1;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //[tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:2.0/255.0 green:90.0/255.0 blue:180.0/255.0 alpha:1.0];//[UIColor colorWithRed:245.0/255.0 green:231.0/255.0 blue:34.0/255.0 alpha:1.0];
    [self cancelButton:nil];
    selectedIndexPath = indexPath;
    peopleAtMeetingArray = [[NSMutableArray alloc]initWithArray:[self.scheduleDetailsArray objectAtIndex:indexPath.section]];
    
    NSDictionary *scheduleDict = [[self.scheduleDetailsArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    meetingDescription.text = [scheduleDict objectForKey:@"Description"];//scheduleData.description;
    eventName.text = [scheduleDict objectForKey:@"Topic"];//scheduleData.duration;
    facilitator.text = [scheduleDict objectForKey:@"Facilitator"];
    place.text = [scheduleDict objectForKey:@"Place"];
    meetingTime.text = [scheduleDict objectForKey:@"Time"];//scheduleData.longTime;
    NSArray *namesArray = nil;
    NSArray *imagesArray = nil;
    NSInteger numberOfImages;
    
    imagesArray = [[NSArray alloc]initWithArray:[scheduleDict objectForKey:@"PeopleImage"]];
    namesArray = [[NSArray alloc] initWithArray:[scheduleDict objectForKey:@"People"]];
    numberOfImages = [[scheduleDict objectForKey:@"PeopleImage"] count];
    
    if ([imagesArray count] == 0) {
        noImagesLabel.hidden = NO;
    }
    else{
        noImagesLabel.hidden = YES;
    }
    
    [self addImagesToScrollView:imagesArray withNames:namesArray];
    peopleImagesScrollView.contentSize = CGSizeMake(120*numberOfImages+5*(numberOfImages + 1), peopleImagesScrollView.frame.size.height);
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    //[tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor whiteColor];
}
-(void)addImagesToScrollView:(NSArray *)imageArray withNames:(NSArray *)namesArray{
    if(peopleImagesScrollView!=nil)
    {
        while ([peopleImagesScrollView.subviews count] > 0) {
            //NSLog(@"subviews Count=%d",[[myScrollView subviews]count]);
            [[[peopleImagesScrollView subviews] objectAtIndex:0] removeFromSuperview];
        }
        peopleImagesScrollView.contentSize = peopleImagesScrollView.frame.size;
    }
    NSInteger count = [imageArray count];
    for (int i=0; i<count; i++) {
        //  NSLog(@"%@ image name",[imageArray objectAtIndex:i]);
        UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[imageArray objectAtIndex:i]]];
        if (image.image == nil) {
            image.image = [UIImage imageNamed:@"PROFILE_IMG.png"];
        }
        image.frame = CGRectMake(i*150, 20, 130 , 130);
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(i*150, 160, 120, 20)];
        label.adjustsFontSizeToFitWidth = YES;
        label.textAlignment = UITextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:16.0];
        label.text = [namesArray objectAtIndex:i];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:0.0/255.0 green:57.0/255.0 blue:181.0/255.0 alpha:1.0];
        //image.layer.cornerRadius = 5;
        image.layer.masksToBounds = YES;
        [image.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [image.layer setBorderWidth: 1.0];
        
        [peopleImagesScrollView addSubview:image];
        [peopleImagesScrollView addSubview:label];
    }
}


-(IBAction)presentationButton:(id)sender
{
    [UIView transitionWithView:self.animationContainer
					  duration:1
					   options:UIViewAnimationOptionTransitionCrossDissolve
					animations:^{  [self.agendaView setHidden:TRUE];[self.presentationView setHidden:FALSE]; }
					completion:NULL];
}

-(IBAction)cancelButton:(id)sender
{
    [UIView transitionWithView:self.animationContainer
					  duration:0.8
					   options:UIViewAnimationOptionTransitionCrossDissolve
					animations:^{  [self.presentationView setHidden:TRUE];[self.agendaView setHidden:FALSE];; }
					completion:NULL];
}

@end
