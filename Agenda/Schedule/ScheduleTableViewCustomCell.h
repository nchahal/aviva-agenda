//
//  ScheduleTableViewCustomCell.h
//  Agenda
//
//  Created by Achu on 26/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleTableViewCustomCell : UITableViewCell
{
    IBOutlet UILabel *meeting;
    IBOutlet UILabel *place;
    IBOutlet UILabel *time;
    IBOutlet UIImageView *image;
}
@property (nonatomic,retain) UILabel *meeting;
@property (nonatomic,retain) UILabel *place;
@property (nonatomic,retain) UILabel *time;
@property (nonatomic,retain) UIImageView *image;
@end