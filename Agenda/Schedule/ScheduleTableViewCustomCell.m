//
//  ScheduleTableViewCustomCell.m
//  Agenda
//
//  Created by Achu on 26/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ScheduleTableViewCustomCell.h"

@implementation ScheduleTableViewCustomCell

@synthesize meeting,place,time,image;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if (selected) {
        for (UILabel * lbl in self.contentView.subviews) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                ((UILabel*)lbl).textColor = [UIColor whiteColor];
            }
        }
        self.meeting.textColor = [UIColor colorWithRed:245.0/255.0 green:231.0/255.0 blue:34.0/255.0 alpha:1.0];
        
        self.contentView.backgroundColor = [UIColor colorWithRed:2.0/255.0 green:90.0/255.0 blue:180.0/255.0 alpha:1.0];
    }
    else{
        for (UILabel * lbl in self.contentView.subviews) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                ((UILabel*)lbl).textColor = [UIColor blackColor];
            }
        }
        self.meeting.textColor = [UIColor colorWithRed:2.0/255.0 green:90.0/255.0 blue:180.0/255.0 alpha:1.0];
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    
}

@end

