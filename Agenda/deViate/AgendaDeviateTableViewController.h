//
//  AgendaDeviateTableViewController.h
//  Agenda
//
//  Created by TCS on 08/08/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgendaDeviateTeamDetailViewController.h"

@interface AgendaDeviateTableViewController : UITableViewController

@property (strong, nonatomic) AgendaDeviateTeamDetailViewController *deviateTeamDetailViewController;

@end
