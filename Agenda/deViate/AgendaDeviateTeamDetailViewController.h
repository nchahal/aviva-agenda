//
//  AgendaDeviateTeamDetailViewController.h
//  Agenda
//
//  Created by TCS on 08/08/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaDeviateTeamDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (retain, nonatomic) id teamDetailItem;

@end
