//
//  AgendaDeviateTeamDetailViewController.m
//  Agenda
//
//  Created by TCS on 08/08/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import "AgendaDeviateTeamDetailViewController.h"

@interface AgendaDeviateTeamDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblTeamName;
@property (weak, nonatomic) IBOutlet UILabel *lblIdeaName;
@property (weak, nonatomic) IBOutlet UITextView *txtIdeaDescription;

@end

@implementation AgendaDeviateTeamDetailViewController

-(void)setTeamDetailItem:(id)newTeamDetailItem
{
    if (_teamDetailItem != newTeamDetailItem) {
        _teamDetailItem = newTeamDetailItem;
        NSLog(@"new %@", newTeamDetailItem);
        // Update the view.
        self.lblTeamName.text = [newTeamDetailItem objectForKey:@"Team Name"];
        self.lblIdeaName.text = [newTeamDetailItem objectForKey:@"Idea Name"];
        self.txtIdeaDescription.text = [newTeamDetailItem objectForKey:@"Idea Description"];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
