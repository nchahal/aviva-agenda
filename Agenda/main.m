//
//  main.m
//  Agenda
//
//  Created by TCS - Aviva on 29/04/14.
//  Copyright (c) 2014 TCS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AgendaAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AgendaAppDelegate class]));
    }
}
